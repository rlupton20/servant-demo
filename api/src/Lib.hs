{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DeriveGeneric #-}
{-|
Module      : Lib
Description : Lib's main module

This is a haddock comment describing your library
For more information on how to write Haddock comments check the user guide:
<https://www.haskell.org/haddock/doc/html/index.html>
-}
module Lib where

import Lib.Prelude

import Servant
import Data.Aeson (FromJSON, ToJSON)
import Data.Swagger (ToSchema)
import qualified Data.Text.Lazy as L

data Store = Store { value :: Text } deriving (Eq, Show, Generic)

instance FromJSON Store
instance ToJSON Store
instance ToSchema Store

data Rule = Rule { name :: Text
                 , id :: Integer } deriving (Eq, Show, Generic)

instance FromJSON Rule
instance ToJSON Rule
instance ToSchema Rule


type API = "swagger" :> Get '[PlainText] L.Text
  :<|> "store" :> Capture "key" Text :> ReqBody '[JSON] Store :> Post '[PlainText] NoContent
  :<|> "rules" :> Get '[JSON] [Rule]

proxyAPI :: Proxy API
proxyAPI = Proxy
