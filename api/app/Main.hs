module Main where

import Servant
import Servant.Swagger
import Data.Aeson (encode, decode)
import qualified Data.Text.Lazy.Encoding as L
import qualified Data.Text.Encoding as T
import qualified Data.Text.Lazy as L
import Network.Wai.Handler.Warp (run)
import qualified Database.Memcache.Client as M
import qualified Database.Etcd as E

import Protolude
import Lib


main :: IO ()
main = do
  run 8081 application

application :: Application
application = serve proxyAPI server

server :: Server API
server = swagger :<|> store :<|> rules

swagger :: Handler L.Text
swagger = return $ L.decodeUtf8.encode $ toSwagger proxyAPI

rules :: Handler [Rule]
rules = liftIO $ do
  let etcd = E.etcd "http://db:2379/"
  text <- ( E.runEtcd etcd $ E.get "rules/" :: IO (Maybe L.Text))
  let rulesM = text >>= decode.( L.encodeUtf8 )
  return $ maybe [] identity rulesM

store :: Text -> Store -> Handler NoContent
store key object = do
  _ <- liftIO $ do
    mc <- M.newClient [ M.def { M.ssHost = "job-store" } ] M.def
    M.set mc key' value' 0 0
  return NoContent
  where
    key' = T.encodeUtf8 key
    value' = T.encodeUtf8 (value object)
